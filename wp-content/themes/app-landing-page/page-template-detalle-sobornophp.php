<?php
   /**
    * Template Name: Detalle Soborno
    * The template for displaying all single posts.
    *
    * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
    *
    * @package App_Landing_Page
    */
   
   get_header(); ?>
<div id="primary" class="content-area">
	<h4>Conoce el detale de las denuncias hechas por los ciudadanos</h4>
   <main id="main" class="site-main" role="main">
      <div class="containter-admin">
         <div class="row">
            <ul class="collapsible" data-collapsible="accordion">
               <li>
                  <div class="collapsible-header"><i class="material-icons">sentiment_dissatisfied</i>Municipalidad de Ica</div>
                  <div class="collapsible-body">
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Soborno en Mesa de Partes</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Solicitud de dinero para tramites irregulares, para obtener la certificación defesnsa civil.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Abuso de autoridad</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Insistencia en funcionarios publicos para obligar a los ciudadanos a ceder sus estacionamientos.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Soborno en Seguridad Ciudad.</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Solicitud de pagos adicionales para enviar patrulleros a un vencidario.
                        </div>
                     </div>
                  </div>
               </li>
               <li>
                  <div class="collapsible-header"><i class="material-icons">sentiment_very_dissatisfied</i>Ministerio de Educación</div>
                  <div class="collapsible-body">
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Soborno en Mesa de Partes</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Solicitud de dinero para tramites irregulares, para obtener la certificación defesnsa civil.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Abuso de autoridad</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Insistencia en funcionarios publicos para obligar a los ciudadanos a ceder sus estacionamientos.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Soborno en Seguridad Ciudad.</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Solicitud de pagos adicionales para enviar patrulleros a un vencidario.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Soborno en Mesa de Partes</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Solicitud de dinero para tramites irregulares, para obtener la certificación defesnsa civil.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Abuso de autoridad</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Insistencia en funcionarios publicos para obligar a los ciudadanos a ceder sus estacionamientos.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Soborno en Seguridad Ciudad.</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Solicitud de pagos adicionales para enviar patrulleros a un vencidario.
                        </div>
                     </div>
                  </div>
               </li>
               <li>
                  <div class="collapsible-header"><i class="material-icons">sentiment_very_dissatisfied</i>Gobierno Regional de la Libertad</div>
                  <div class="collapsible-body">
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Soborno en Mesa de Partes</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Solicitud de dinero para tramites irregulares, para obtener la certificación defesnsa civil.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Abuso de autoridad</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Insistencia en funcionarios publicos para obligar a los ciudadanos a ceder sus estacionamientos.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Soborno en Seguridad Ciudad.</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Solicitud de pagos adicionales para enviar patrulleros a un vencidario.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Soborno en Mesa de Partes</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Solicitud de dinero para tramites irregulares, para obtener la certificación defesnsa civil.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Abuso de autoridad</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Insistencia en funcionarios publicos para obligar a los ciudadanos a ceder sus estacionamientos.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Soborno en Seguridad Ciudad.</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Solicitud de pagos adicionales para enviar patrulleros a un vencidario.
                        </div>
                     </div>
                  </div>
               </li>
               <li>
                  <div class="collapsible-header"><i class="material-icons">sentiment_very_dissatisfied</i>Hospital de la Solidaridad</div>
                  <div class="collapsible-body">
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Soborno en Mesa de Partes</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Solicitud de dinero para tramites irregulares, para obtener la certificación defesnsa civil.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Abuso de autoridad</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Insistencia en funcionarios publicos para obligar a los ciudadanos a ceder sus estacionamientos.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Soborno en Seguridad Ciudad.</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Solicitud de pagos adicionales para enviar patrulleros a un vencidario.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Soborno en Mesa de Partes</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Solicitud de dinero para tramites irregulares, para obtener la certificación defesnsa civil.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Abuso de autoridad</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Insistencia en funcionarios publicos para obligar a los ciudadanos a ceder sus estacionamientos.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Soborno en Seguridad Ciudad.</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Solicitud de pagos adicionales para enviar patrulleros a un vencidario.
                        </div>
                     </div>
                  </div>
               </li>
               <li>
                  <div class="collapsible-header"><i class="material-icons">sentiment_very_dissatisfied</i>Comisaria de Santa Catalina</div>
                  <div class="collapsible-body">
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Soborno en Mesa de Partes</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Solicitud de dinero para tramites irregulares, para obtener la certificación defesnsa civil.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Abuso de autoridad</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Insistencia en funcionarios publicos para obligar a los ciudadanos a ceder sus estacionamientos.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Soborno en Seguridad Ciudad.</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Solicitud de pagos adicionales para enviar patrulleros a un vencidario.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Soborno en Mesa de Partes</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Solicitud de dinero para tramites irregulares, para obtener la certificación defesnsa civil.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Abuso de autoridad</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Insistencia en funcionarios publicos para obligar a los ciudadanos a ceder sus estacionamientos.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Soborno en Seguridad Ciudad.</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Solicitud de pagos adicionales para enviar patrulleros a un vencidario.
                        </div>
                     </div>
                  </div>
               </li>
               <li>
                  <div class="collapsible-header"><i class="material-icons">sentiment_very_dissatisfied</i>Tercer Juzgado Penal de Lima</div>
                  <div class="collapsible-body">
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Soborno en Mesa de Partes</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Solicitud de dinero para tramites irregulares, para obtener la certificación defesnsa civil.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Abuso de autoridad</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Insistencia en funcionarios publicos para obligar a los ciudadanos a ceder sus estacionamientos.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Soborno en Seguridad Ciudad.</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Solicitud de pagos adicionales para enviar patrulleros a un vencidario.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Soborno en Mesa de Partes</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Solicitud de dinero para tramites irregulares, para obtener la certificación defesnsa civil.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Abuso de autoridad</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Insistencia en funcionarios publicos para obligar a los ciudadanos a ceder sus estacionamientos.
                        </div>
                     </div>
                     <div class="exp-container">
                        <div class="exp-item exp-containter-case">
                           <p>Soborno en Seguridad Ciudad.</p>
                        </div>
                        <div class="exp-item exp-containter-detail">
                           Solicitud de pagos adicionales para enviar patrulleros a un vencidario.
                        </div>
                     </div>
                  </div>
               </li>
            </ul>
         </div>
      </div>
   </main>
   <!-- #main -->
</div>
<!-- #primary -->
<?php
get_footer();
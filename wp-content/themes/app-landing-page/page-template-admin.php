<?php
/**
 * Template Name: Admin Page
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package App_Landing_Page
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">


			<div class="containter-admin">
				
			<div class="row">
				<div class="col l6">
					<div class="card">
						<div class="card-image waves-effect waves-block waves-light">
							<img class="activator" src="https://andina-learning.nyc3.digitaloceanspaces.com/Defender/pnp1.jpg">
						</div>
						<div class="card-content">
							<span class="card-title activator grey-text text-darken-4">División PNP Anticorrupción<i class="material-icons right">more_vert</i></span>
							<p><a href="#">Ver detalle</a></p>
						</div>
						<div class="card-reveal">
							<span style="margin-bottom: 35px" class="card-title grey-text text-darken-4">División PNP Anticorrupción<i class="material-icons right">close</i></span>
							
							<h5>153 denuncias asignadas</h5>
							<h5>53 casos detectados y procesados</h5>
							<p>La Policia Nacional del Perú realiza el seguimiento para delitos tipificados en el codigo penal.</p>
							<a class="waves-effect waves-light btn">Casos detallados y Estadistica</a>
						</div>
					</div>

					<div class="card">
						<div class="card-image waves-effect waves-block waves-light">
							<img class="activator" src="https://andina-learning.nyc3.digitaloceanspaces.com/Defender/contraloria.jpg">
						</div>
						<div class="card-content">
							<span class="card-title activator grey-text text-darken-4">Contraloría General de la Rep.<i class="material-icons right">more_vert</i></span>
							<p><a href="#">This is a link</a></p>
						</div>
						<div class="card-reveal">
							<span style="margin-bottom: 35px" class="card-title grey-text text-darken-4">Contraloría General de la Rep.<i class="material-icons right">close</i></span>
							<h5>153 denuncias asignadas</h5>
							<h5>53 casos detectados y procesados</h5>
							<p>La Policia Nacional del Perú realiza el seguimiento para delitos tipificados en el codigo penal.</p>
							<a class="waves-effect waves-light btn">Casos detallados y Estadistica</a>
						</div>
					</div>
				</div>

				<div class="col l6">
					<div class="card">
						<div class="card-image waves-effect waves-block waves-light">
							<img class="activator" src="https://andina-learning.nyc3.digitaloceanspaces.com/Defender/ministerio-pub.jpg">
						</div>
						<div class="card-content">
							<span class="card-title activator grey-text text-darken-4">Ministerio Público<i class="material-icons right">more_vert</i></span>
							<p><a href="#">This is a link</a></p>
						</div>
						<div class="card-reveal">
							<span style="margin-bottom: 35px" class="card-title grey-text text-darken-4">Ministerio Público<i class="material-icons right">close</i></span>
							<h5>153 denuncias asignadas</h5>
							<h5>53 casos detectados y procesados</h5>
							<p>La Policia Nacional del Perú realiza el seguimiento para delitos tipificados en el codigo penal.</p>
							<a class="waves-effect waves-light btn">Casos detallados y Estadistica</a>
						</div>
					</div>

					<div class="card">
						<div class="card-image waves-effect waves-block waves-light">
							<img class="activator" src="https://andina-learning.nyc3.digitaloceanspaces.com/Defender/procuraduria.jpg">
						</div>
						<div class="card-content">
							<span class="card-title activator grey-text text-darken-4">Procuraduría Esp. en Corrupción<i class="material-icons right">more_vert</i></span>
							<p><a href="#">This is a link</a></p>
						</div>
						<div class="card-reveal">
							<span style="margin-bottom: 35px" class="card-title grey-text text-darken-4">Procuraduría Esp. en Corrupción<i class="material-icons right">close</i></span>
							<h5>153 denuncias asignadas</h5>
							<h5>53 casos detectados y procesados</h5>
							<p>La Policia Nacional del Perú realiza el seguimiento para delitos tipificados en el codigo penal.</p>
							<a class="waves-effect waves-light btn">Casos detallados y Estadistica</a>
						</div>
					</div>
				</div>	

			</div>


			</div>


		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

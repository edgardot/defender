<?php
/**
 * Template Name: Estadistica
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package App_Landing_Page
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">


			<div class="containter-admin">
				
			<div class="row">
				
				<div class="col l4">
					<div class="col l12 s12 m7">
						<h5 class="header">Soborno</h5>
						<div class="card horizontal">
							<div class="card-image">
								<img src="https://andina-learning.nyc3.digitaloceanspaces.com/Defender/soborno.jpg">
							</div>
							<div class="card-stacked">
								<div class="card-content">
									<p>Se trata cuando entregas dinero, bienes o favores a alguien para lograr un fin especifico, que puedo o no corresponderte.</p>
								</div>
								<div class="card-action">
									<a href="http://beta.tupino.com/defender/detalle">Ir a detalle</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col l4">
					<div class="col l12 s12 m7">
						<h5 class="header">Peculado</h5>
						<div class="card horizontal">
							<div class="card-image">
								<img src="https://andina-learning.nyc3.digitaloceanspaces.com/Defender/peculado.jpg">
							</div>
							<div class="card-stacked">
								<div class="card-content">
									<p>Básicamente la apropiación de bienes del estado por parte de un funcionario publico a favor del mismo.</p>
								</div>
								<div class="card-action">
									<a href="http://beta.tupino.com/defender/detalle">Ir a detalle</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col l4">
					<div class="col l12 s12 m7">
						<h5 class="header">Malveración de Fondos</h5>
						<div class="card horizontal">
							<div class="card-image">
								<img src="https://andina-learning.nyc3.digitaloceanspaces.com/Defender/malveracion.jpg">
							</div>
							<div class="card-stacked">
								<div class="card-content">
									<p>Utilización de fondos dinerarios del estado para fines diferentes a los cuales fue incialmente destinado.</p>
								</div>
								<div class="card-action">
									<a href="http://beta.tupino.com/defender/detalle">Ir a detalle</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">	
				<div class="col l4">
					<div class="col l12 s12 m7">
						<h5 class="header">Abuso de Autoridad</h5>
						<div class="card horizontal">
							<div class="card-image">
								<img src="https://andina-learning.nyc3.digitaloceanspaces.com/Defender/abusoautoridad.jpg">
							</div>
							<div class="card-stacked">
								<div class="card-content">
									<p>La utilización del cargo para lograr presionar a otros y realizar actividades en contra de su voluntad.</p>
								</div>
								<div class="card-action">
									<a href="http://beta.tupino.com/defender/detalle">Ir a detalle</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col l4">
					<div class="col l12 s12 m7">
						<h5 class="header">Colusión</h5>
						<div class="card horizontal">
							<div class="card-image">
								<img src="https://andina-learning.nyc3.digitaloceanspaces.com/Defender/colusion.jpg">
							</div>
							<div class="card-stacked">
								<div class="card-content">
									<p>Hecho por el cual existe asociación de funcionario o privados con este para perjudicar a un tecero.</p>
								</div>
								<div class="card-action">
									<a href="http://beta.tupino.com/defender/detalle">Ir a detalle</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col l4">
					<div class="col l12 s12 m7">
						<h5 class="header">Negociación Indebido</h5>
						<div class="card horizontal">
							<div class="card-image">
								<img src="https://andina-learning.nyc3.digitaloceanspaces.com/Defender/aprovechamiento.jpg">
							</div>
							<div class="card-stacked">
								<div class="card-content">
									<p>La utilziacón al acceso de información privada y sensible en favo de lograr condicones economicas para si o terceros (contratos) .</p>
								</div>
								<div class="card-action">
									<a href="http://beta.tupino.com/defender/detalle">Ir a detalle</a>
								</div>
							</div>
						</div>
					</div>
				</div>


			</div>

			<div class="row">
				<div id="container" style="min-width: 400px; max-width: 800px; height: 400px; margin: 0 auto"></div>
			</div>


			</div>


		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

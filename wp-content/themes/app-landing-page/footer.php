<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package App_Landing_Page
 */

    /**
     * After Content
     * 
     * @hooked app_landing_page_content_end - 20
    */
    do_action( 'app_landing_page_after_content' );
    

    /**
     * App Landing Page Footer
     * 
     * @hooked app_landing_page_footer_start  - 20
     * @hooked app_landing_page_footer_widgets   - 30
     * @hooked app_landing_page_footer_credit - 40
     * @hooked app_landing_page_footer_end    - 50
    */
	do_action( 'app_landing_page_footer' ); 
    
    /**
	 * After Footer
     * 
     * @hooked app_landing_page_page_end - 20
	 */
    do_action( 'app_landing_page_page_end' );
    

wp_footer(); ?>
<!-- Compiled and minified JavaScript -->
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
</body>
</html>


jQuery(document).ready(function($){

	$("body").niceScroll({
		cursorcolor: "#5fbd3e",
		zindex: "999999",
		cursorborder: "none",
		cursoropacitymin: "0",
		cursoropacitymax: "1",
		cursorwidth: "8px",
		cursorborderradius: "0px;"
	});

	/* Equal Height */
     $('.section-5 .col').matchHeight({
        byRow: true,
        property: 'height',
        target: null,
        remove: false
    });
    
    /* Date Picker */
    $( "#datepicker" ).datepicker();

	$('#responsive-menu-button').sidr({
    	name: 'sidr-main',
    	source: '#site-navigation',
    	side: 'right'
    });

	var date_in = app_landing_page_data.date;

   	$('#days').countdown( date_in, function(event) {
  		$(this).html(event.strftime('%D'));
	});
	$('#hours').countdown( date_in, function(event) {
  		$(this).html(event.strftime('%H'));
	});
	$('#minutes').countdown(date_in, function(event) {
  		$(this).html(event.strftime('%M'));
	});
	$('#seconds').countdown(date_in, function(event) {
  		$(this).html(event.strftime('%S'));
	});
	
	//Event CountDown------------
	new WOW().init();

// ADD EVENTS


	

		



Highcharts.chart('container', {

    chart: {
        polar: true,
        type: 'line'
    },

    title: {
        text: 'Denuncias presentadas',
        x: -80
    },

    pane: {
        size: '80%'
    },

    xAxis: {
        categories: ['Mun. Distritales', 'Gob. Regionales', 'PNP', 'Minsterios',
                'Org. Autonomos', 'Pd. Judicial'],
        tickmarkPlacement: 'on',
        lineWidth: 0
    },

    yAxis: {
        gridLineInterpolation: 'polygon',
        lineWidth: 0,
        min: 0
    },

    tooltip: {
        shared: true,
        pointFormat: '<span style="color:{series.color}">{series.name}: <b>${point.y:,.0f}</b><br/>'
    },

    legend: {
        align: 'right',
        verticalAlign: 'top',
        y: 70,
        layout: 'vertical'
    },

    series: [{
        name: 'Peculado',
        data: [53, 20, 16, 23, 25, 75],
        pointPlacement: 'on'
    }, {
        name: 'Soborno',
        data: [52, 39, 42, 13, 12, 14],
        pointPlacement: 'on'
    }]

});

});
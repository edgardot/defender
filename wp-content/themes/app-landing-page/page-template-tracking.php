<?php
	/**
	 * Template Name: Tracking
	 * The template for displaying all single posts.
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
	 *
	 * @package App_Landing_Page
	 */
	
	get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<!-- PROGRESS -->
		<!-- TACKING NUMBER -->
		<!-- PROGRESS BAR -->
		<ul class="progressbar">
			<li class="active">Vigilancia</li>
			<li>Investigación</li>
			<li>Resuelto</li>
		</ul>

		<div class="container">
			<!-- TABLE TRACKING BOX -->
			<div class="tracking-box">
				<h1>Número de seguimiento: <span> MP-00015</span></h1>
				<div class="container-table">
					<table>
						<thead></thead>
						<tbody>
							<tr>
								<td class="td-name">Denunciante</td>
								<td>Edgardo Tupiño</td>
							</tr>
							<tr>
								<td class="td-name">DNI</td>
								<td>44700200</td>
							</tr>
							<tr>
								<td class="td-name">Telefono</td>
								<td>+51960150481</td>
							</tr>
						</tbody>
					</table>
					<div class="separator"></div>
					<table>
						<thead></thead>
						<tbody>
							<tr>
								<td class="td-name">Ubicación</td>
								<td>Municipalidad de Miraflores</td>
							</tr>
							<tr>
								<td class="td-name">Ciudad</td>
								<td>Lima</td>
							</tr>
							<tr>
								<td class="td-name">Hechos</td>
								<td>Resulta que un funcionario publico se acero a mi local, me impidio el funcionamiento de mi neogico por solicitarme a cambio dinero para la obtención de un certificado fantasma.</td>
							</tr>
							<tr>
								<td class="td-name">Áreas involucradas</td>
								<td>Mesa de Partes, Oficina de seguridad ciudadana</td>
							</tr>
							<tr>
								<td class="td-name">Medios Probatorios</td>
								<td> <a href="#">Audios</a></td>
							</tr>
						</tbody>
					</table>
					<div class="separator"></div>
					<table>
						<thead></thead>
						<tbody>
							<tr>
								<td class="td-name">Entidad a cargo</td>
								<td>Ministerio Público (Físcalia de la Nación)</td>
							</tr>
							<tr>
								<td class="td-name">Oficina a cargo</td>
								<td>Oficina de seguimiento distrital  </td>
							</tr>
						</tbody>
					</table>
				</div>
				<a class="waves-effect waves-light btn">Imprimir</a>
				<!-- <a class="waves-effect waves-light btn">Solicitar mas información</a> -->
			</div>
			<!-- END TRACKING BOX -->
		</div>
	</main>
	<!-- #main -->
</div>
<!-- #primary -->
<?php
get_footer();